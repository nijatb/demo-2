#!/usr/bin/env sh

for i in {1..10}
do
    if wget --quiet -O - localhost/actuator/health | grep "UP" > /dev/null
    then
        exit 0
    else
        sleep 30
    fi
done
exit 1
